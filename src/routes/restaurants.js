const router = require('express').Router()

const RestaurantsController = require('../controllers/restaurants')

router.route('/:id')
    .get(RestaurantsController.get)
    .patch(RestaurantsController.update)
    .delete(RestaurantsController.delete)

router.route('/:id/menus')
    .get(RestaurantsController.getMenus)
    .post(RestaurantsController.addMenu)

router.route('/:id/rooms')
    .get(RestaurantsController.getRooms)
    .post(RestaurantsController.addRoom)

router.route('/:id/reservations')
    .get(RestaurantsController.getReservations)
    .post(RestaurantsController.addReservation)

router.route('/:id/schedules')
    .get(RestaurantsController.getSchedules)

router.route('/')
    .get(RestaurantsController.getAll)
    .post(RestaurantsController.create)

module.exports = router