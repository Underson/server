const router = require('express').Router()

const AllergensController = require('../controllers/allergens')

router.route('/')
    .get(AllergensController.getAll)
    .post(AllergensController.create)

router.route('/:id')
    .get(AllergensController.get)
    .put(AllergensController.update)
    .delete(AllergensController.delete)

module.exports = router