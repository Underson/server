const router = require('express').Router()

const MenusController = require('../controllers/menus')
const MenuTypesController = require('../controllers/menu_types')

router.route('/types')
    .get(MenuTypesController.getAll)
    .post(MenuTypesController.create)

router.route('/:id')
    .get(MenusController.get)
    .put(MenusController.update)
    .delete(MenusController.delete)

router.route('/:id/products')
    .get(MenusController.getProducts)
    .post(MenusController.addProduct)

router.route('/:id/products/:productId')
    .delete(MenusController.deleteProduct)

router.route('/')
    .post(MenusController.create)

module.exports = router