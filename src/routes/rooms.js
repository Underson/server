const router = require('express').Router()

const RoomsController = require('../controllers/rooms')

router.route('/:id')
    .get(RoomsController.get)
//     .patch(
//         RoomsController.update
//     )
//     .delete(
//         RoomsController.delete
//     )

router.route('/:id/tables')
    .get(
        RoomsController.getTables
    )
//     .post(
//         RoomsController.addTable
//     )

module.exports = router