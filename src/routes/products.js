const Router = require('express').Router()

const ProductsController = require('../controllers/products')

Router.route('/')
    .get(ProductsController.getAll)

module.exports = Router