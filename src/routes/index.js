const allergens = require('./allergens')
const menus = require('./menus')
const restaurants = require('./restaurants')
const rooms = require('./rooms')
const ingredients = require('./ingredients')
const products = require('./products')

module.exports = app => {
    app.use('/allergens', allergens)
    .use('/menus', menus)
    .use('/restaurants', restaurants)
    .use('/rooms', rooms)
    .use('/ingredients', ingredients)
    .use('/products', products)
}