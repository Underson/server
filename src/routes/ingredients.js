const router = require('express').Router()

const IngredientsController = require('../controllers/ingredients')
const IngredientTypesController = require('../controllers/ingredientTypes')

router.route('/')
    .get(IngredientsController.getAll)
    .post(IngredientsController.create)

router.route('/types')
    .get(IngredientTypesController.getAll)
    .post(IngredientTypesController.create)

router.route('/types/:id')
    .get(IngredientTypesController.get)
    .patch(IngredientTypesController.update)
    .delete(IngredientTypesController.delete)

router.route('/:id')
    .get(IngredientsController.get)
    .patch(IngredientsController.update)
    .delete(IngredientsController.delete)

module.exports = router