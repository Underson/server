const PORT = 3001

const cors = require('cors')
const helmet = require('helmet')
const compression = require('compression')
const express = require('express')
const winston = require('winston')
const morgan = require('morgan')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const wsServer = require('./wsServer')(io)

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
})

logger.stream = {
    write: function(message, encoding) {
        logger.info(message)
    }
};
  
// app.use(morgan('dev'))
app.use(morgan('combined', {
    stream: logger.stream
}))

.use(express.json())
.use(cors())
.use(compression())
.use(helmet())
.use(express.urlencoded({ extended: false }))

require('./routes')(app)

app.use(function(err, req, res, next) {
    logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  
    res.status(err.status || 500).send()
})

server.listen(PORT, () => console.log(`Serveur lancé sur le port : ${PORT}`))