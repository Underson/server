/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('productsCategories', {
    product: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'id'
      },
      field: 'product'
    },
    categorie: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'categorie'
    }
  }, {
    tableName: 'products_categories'
  });
};
