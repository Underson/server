/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ingredientsAllergens', {
    allergen: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'allergens',
        key: 'id'
      },
      field: 'allergen'
    },
    ingredient: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ingredients',
        key: 'id'
      },
      field: 'ingredient'
    }
  }, {
    tableName: 'ingredients_allergens'
  });
};
