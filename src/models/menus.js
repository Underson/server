/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('menus', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'price'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      field: 'active'
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'description'
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      field: 'date'
    },
    image: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'images',
        key: 'id'
      },
      field: 'image'
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'menu_types',
        key: 'id'
      },
      field: 'type'
    }
  }, {
    tableName: 'menus'
  });
};
