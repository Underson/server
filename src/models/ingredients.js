/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Ingredients', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'ingredient_types',
        key: 'id'
      },
      field: 'type'
    }
  }, {
    tableName: 'ingredients'
  });
};
