/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('restaurants', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'address'
    },
    zip: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'zip'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'city'
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'latitude'
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'longitude'
    },
    nbPlaces: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'nb_places'
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'description'
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'phone'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'email'
    }
  }, {
    tableName: 'restaurants'
  });
};
