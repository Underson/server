/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Rooms = sequelize.define('Rooms', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    restaurant: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'restaurant'
    }
  }, {
    tableName: 'rooms'
  })

  Rooms.associate = models => {
    Rooms.belongsTo(models.restaurants, {
      foreignKey: 'restaurant',
      targetKey: 'id',
      as: 'Restaurant'
    })
  }

  return Rooms
}