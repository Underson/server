/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const MenusProducts = sequelize.define('MenusProducts', {
    menu: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'menu'
    },
    product: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'product'
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '1',
      field: 'quantity'
    }
  }, {
    tableName: 'menus_products'
  })

  MenusProducts.associate = models => {
    MenusProducts.belongsTo(models.Products, {
      foreignKey: 'product',
      targetKey: 'id',
      as: 'Product'
    })
  }

  return MenusProducts
};
