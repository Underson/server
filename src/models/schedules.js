/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Schedules', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    startMorning: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'start_morning'
    },
    endMorning: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'end_morning'
    },
    startAfternoon: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'start_afternoon'
    },
    endAfternoon: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'end_afternoon'
    },
    date: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'date'
    },
    restaurant: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'restaurants',
        key: 'id'
      },
      field: 'restaurant'
    }
  }, {
    tableName: 'schedules'
  })
}
