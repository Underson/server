/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Products = sequelize.define('Products', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'price'
    },
    weight: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'weight'
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'description'
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      field: 'date'
    },
    image: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  }, {
    tableName: 'products'
  })

  Products.associate = models => {
    Products.hasMany(models.ProductsIngredients, {
      foreignKey: 'product',
      sourceKey: 'id',
      as: 'Ingredients'
    })
  }

  return Products
}
