/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('categories', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '0',
      field: 'order'
    }
  }, {
    tableName: 'categories'
  });
};
