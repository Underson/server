/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('allergens', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    }
  }, {
    tableName: 'allergens'
  });
};
