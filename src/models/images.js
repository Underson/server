/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('images', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'url'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name'
    }
  }, {
    tableName: 'images'
  });
};
