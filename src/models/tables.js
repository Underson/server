/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Tables = sequelize.define('Tables', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id',
      autoIncrement: true
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'number'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name'
    },
    room: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'room'
    }
  }, {
    tableName: 'tables'
  })

  Tables.associate = models => {
    Tables.belongsTo(models.Rooms, {
      foreignKey: 'room',
      targetKey: 'id',
      as: 'Room'
    })
  }

  return Tables
};
