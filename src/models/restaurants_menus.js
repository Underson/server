/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const RestaurantsMenus =  sequelize.define('RestaurantsMenus', {
    restaurant: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'restaurant'
    },
    menu: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'menu'
    }
  }, {
    tableName: 'restaurants_menus'
  })

  RestaurantsMenus.associate = models => {
    RestaurantsMenus.belongsTo(models.menus, {
      foreignKey: 'menu',
      targetKey: 'id',
      as: 'Menu'
    })
  }

  return RestaurantsMenus
};
