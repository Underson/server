/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Reservations = sequelize.define('Reservations', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    nbPeoples: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'nb_peoples'
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'firstname'
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'lastname'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'email'
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'phone'
    },
    table: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'table'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date'
    }
  }, {
    tableName: 'reservations'
  })

  Reservations.associate = models => {
    Reservations.belongsTo(models.Tables, {
      foreignKey: 'table',
      targetKey: 'id',
      as: 'Table'
    })
  }

  return Reservations
};
