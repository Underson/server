/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const ProductsIngredients =  sequelize.define('ProductsIngredients', {
    product: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'products',
        key: 'id'
      },
      field: 'product'
    },
    ingredient: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ingredients',
        key: 'id'
      },
      field: 'ingredient'
    }
  }, {
    tableName: 'products_ingredients'
  })

  ProductsIngredients.associate = models => {
    ProductsIngredients.belongsTo(models.Products, {
      foreignKey: 'product',
      targetKey: 'id'
    })

    ProductsIngredients.belongsTo(models.Ingredients, {
      foreignKey: 'ingredient',
      targetKey: 'id',
      as: 'Ingredient'
    })
  }

  return ProductsIngredients
}
