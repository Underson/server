/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('productsFilters', {
    product: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'products',
        key: 'id'
      },
      field: 'product'
    },
    filter: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'filters',
        key: 'id'
      },
      field: 'filter'
    }
  }, {
    tableName: 'products_filters'
  });
};
