const models = require('../models')

module.exports = {
    getAll: async (req, res, next) => {
        try{
            const ingredients = await models.ingredients.findAll()

            res.json(ingredients)
        }catch(err){
            next(err)
        }
    },
    get: async (req, res, next) => {
        try{
            const ingredient = await models.ingredients.findOne({
                where: {
                    id: req.params.id
                }
            })

            res.json(ingredient)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {name, type} = req.body

            const {id} = await models.ingredients.create({
                name,
                type
            })

            res.status(201).json(id)
        }catch(err){            
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {name, type} = req.body
            const rows = await models.ingredients.update({
                name,
                type
            }, {
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.ingredients.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    }
}