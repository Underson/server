const models = require('../models')

module.exports = {
    get: async (req, res, next) => {
        try{
            const room = await models.Rooms.findOne({
                where: {
                    id: req.params.id
                },
                attributes: {
                    exclude: ['id']
                }
            })

            res.json(room)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {name} = req.body

            const rows = await models.Rooms.update({
                name
            }, {
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.Rooms.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    },
    getTables: async (req, res, next) => {
        try{
            const tables = await models.Tables.findAll({
                attributes: {
                    exclude: ['room']
                },
                where: {
                    room: req.params.id
                }
            })

            res.json(tables)
        }catch(err){
            next(err)
        }
    },
    addTable: async (req, res, next) => {
        try{
            const {number, name} = req.body

            const {id} = await models.Tables.create({
                name,
                number, name,
                room: req.params.id
            })

            res.json(id)
        }catch(err){
            next(err)
        }
    },
}