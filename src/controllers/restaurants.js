const models = require('../models')

module.exports = {
    getAll: async (req, res, next) => {
        try{
            const restaurants = await models.restaurants.findAll()

            res.json(restaurants)
        }catch(err){
            next(err)
        }
    },
    get: async (req, res, next) => {
        try{
            const restaurant = await models.restaurants.findOne({
                attributes: {
                    exclude: ['id']
                }
            })

            res.json(restaurant)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {
                name,
                address, 
                zip,
                city,
                latitude,
                longitude,
                nbPlaces,
                description,
                phone,
                email
            } = req.body
    
            const {id} = await restaurants.create({
                name,
                address, 
                zip,
                city,
                latitude,
                longitude,
                nbPlaces,
                description,
                phone,
                email
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: (req, res, next) => {
        try{
            const {
                name,
                address, 
                zip,
                city,
                latitude,
                longitude,
                nbPlaces,
                description,
                phone,
                email
            } = req.body
    
            const rows = restaurants.update({
                name,
                address, 
                zip,
                city,
                latitude,
                longitude,
                nbPlaces,
                description,
                phone,
                email
            }, {where: {
                id: req.params.id
            }})
            
            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows =await models.restaurants.destroy({where: {
                id: res.params.id
            }})

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    },
    getMenus: async (req, res, next) => {
        try{
            const menus = await models.RestaurantsMenus.findAll({
                where: {
                    restaurant: req.params.id
                },
                attributes: {
                    exclude: ['menu', 'restaurant']
                },
                include: [
                    {
                        model: models.menus,
                        as: 'Menu'
                    }
                ]
            })

            res.json(menus)
        }catch(err){
            next(err)
        }
    },
    addMenu: async (req, res, next) => {
        try{
            const {} = await models.RestaurantsMenus.create({
                restaurant: req.params.id,
                menu: req.body.menu
            })

            res.status(201).json(true)
        }catch(err){
            next(err)
        }
    },
    getRooms: async (req, res, next) => {
        try{
            const rooms = await models.rooms.findAll({
                attributes: {
                    exclude: ['restaurant']
                },
                where: {
                    restaurant: req.params.id
                }
            })

            res.json(rooms)
        }catch(err){
            next(err)
        }
    },
    addRoom: async (req, res, next) => {
        try{
            const {name} = req.body

            const {id} = await models.rooms.create({
                restaurant: req.params.id,
                name
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    getReservations: async (req, res, next) => {
        try{
            const reservations = await models.Reservations.findAll({
                attributes: {
                    exclude: ['table']
                },
                include: [
                    {
                        model: models.Tables,
                        as: 'Table',
                        include: [
                            {
                                model: models.Rooms,
                                as: 'Room',
                                attributes: {
                                    exclude: ['restaurant']
                                },
                                where: {
                                    restaurant: req.params.id
                                }
                            }
                        ]
                    }
                ]
            })

            res.json(reservations)
        }catch(err){
            next(err)
        }
    },
    addReservation: async (req, res, next) => {
        try{
            const {nbPeoples, firstname, lastname, email, phone, table, date} = req.body

            const {id} = await models.Reservations.create({
                nbPeoples,
                firstname,
                lastname,
                email,
                phone,
                table,
                date
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    getSchedules: async (req, res, next) => {
        try{
            const schedules = await models.Schedules.findAll({
                where: {
                    restaurant: req.params.id
                }
            })

            res.json(schedules)
        }catch(err){
            next(err)
        }
    }
}