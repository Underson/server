const models = require('../models')

module.exports = {
    get: async (req, res, next) => {
        try{
            const allergen = await models.allergens.findOne({
                attributes: {
                    exclude: ['id']
                },
                where: {
                    id: req.params.id
                }
            })

            res.json(allergen)
        }catch(err){
            next(err)
        }
    },
    getAll: async (req, res, next) => {
        try{
            const allergens = await models.allergens.findAll()

            res.json(allergens)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {id} = await models.allergens.create({
                name: req.body.name
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const rows = await models.allergens.update({
                name: req.body.name
            }, {
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.allergens.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    }
}