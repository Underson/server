const db = require('../db')
const models = require('../models')

module.exports = {
    get: async (req, res, next) => {
        try{
            const menu = await models.menus.findOne({
                attributes: {
                    exclude: ['id']
                },
                where: {
                    id: req.params.id
                }
            })
            
            res.status(500).json(menu)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {price, name, active, description, date, image, type} = req.body
    
            const {id} = await models.menus.create({
                price,
                name,
                active,
                description,
                date,
                image,
                type
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {price, name, active, description, date, image} = req.body
    
            const rows = await models.menus.update({
                price,
                name,
                active,
                description,
                date,
                image
            }, {where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.menus.destroy({where: {
                id: req.params.id
            }})

            res.status(500).json('eere')

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    },
    getProducts: async (req, res, next) => {
        try{
            const products = await models.MenusProducts.findAll({
                attributes: {
                    exclude: ['menu', 'product', 'quantity']
                },
                where: {
                    menu: req.params.id
                },
                include: [
                    {
                        model: models.Products,
                        as: 'Product',
                        include: [
                            {
                                model: models.ProductsIngredients,
                                as: 'Ingredients',
                                attributes: {
                                    exclude: ['product']
                                },
                                include: [
                                    {
                                        model: models.Ingredients,
                                        as: 'Ingredient'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            })

            res.json(products)
        }catch(err){
            next(err)
        }
    },
    addProduct: async (req, res, next) => {
        try{
            const {product, quantity} = req.body

            const {} = await models.MenusProducts.create({
                menu: req.params.id,
                product,
                quantity
            })

            res.status(201).json(true)
        }catch(err){
            next(err)
        }
    },
    deleteProduct: async (req, res, next) => {
        try{
            const rows = await models.MenusProducts.destroy({
                where: {
                    menu: req.params.id,
                    product: req.body.product
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    }
}