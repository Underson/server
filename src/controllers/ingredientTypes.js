const models = require('../models')

module.exports = {
    getAll: async (req, res, next) => {
        try{
            const ingredientsTypes = await models.ingredientTypes.findAll()

            res.json(ingredientsTypes)
        }catch(err){
            next(err)
        }
    },
    get: async (req, res, next) => {
        try{
            const ingredientType = await models.ingredientTypes.findOne({
                attributes: {
                    exclude: ['id']
                },
                where: {
                    id: req.params.id
                }
            })

            res.json(ingredientType)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {name} = req.body

            const {id} = await models.ingredientTypes.create({
                name
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {name} = req.body
            
            const rows = await models.ingredientTypes.update({
                name
            }, {
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.ingredientTypes.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    }
}