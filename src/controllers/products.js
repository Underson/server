const models = require('../models')

module.exports = {
    getAll: async (req, res, next) => {
        res.json(await models.Products.findAll())
    }
}