const db = require('../db')

module.exports = {
    getAll: (req, res, next) => {
        db.query('select * from menu_types')
        .then(({rows}) => res.json(rows))
        .catch(err => next(err))
    },
    create: (req, res, next) => {
        db.query({
            name: 'crete-menu-type',
            text: `insert into menu_types (name) values ($1) returning id`,
            values: [req.body.name]
        })
        .then(({rows}) => res.json(rows[0].id))
        .catch(err => next(err))
    }
}