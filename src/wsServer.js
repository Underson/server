module.exports = io => {
    const clients = {}
    const sockets = {}

    io.on('connection', socket => {
        if(clients[socket.id]){
            console.log("l'utilisateur est référencé")
        }else{
            sockets[socket.id] = socket;
            clients[socket.id.toString()] = { name: `Thierry ${Object.values(clients).length}`, id: socket.id }
            setTimeout(() => {
                for(let idSocket in sockets) {
                    let temptab = {};
                    for(let id in clients){
                        const client = clients[id];
                        if(idSocket != id){
                            temptab[id] = clients[id];
                        }
                    }
                    sockets[idSocket].emit('users',temptab)
                }
            },500);
        }

        socket.on('send product',(socketId, product)=> {
            if(sockets[socketId] && clients[socketId]){
                sockets[socketId].emit('receive product', product, clients[socketId].name)
            }
        })

        socket.on('disconnect', () => {
            delete clients[socket.id];
            delete sockets[socket.id];
            for(let idSocket in sockets) {
                let temptab = {};
                for(let id in clients){
                    const client = clients[id];
                    if(idSocket != id){
                        temptab[id] = clients[id];
                    }
                }
                sockets[idSocket].emit('users',temptab)
            }
        })
    })
}