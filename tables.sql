drop table if exists allergens cascade;
create table allergens(
    id serial primary key,
    name varchar(100) not null
);

insert into allergens (id, name) values
(1, 'Polènes'),
(2, 'Oeufs');

drop table if exists categories cascade;
create table categories(
    id serial primary key,
    name varchar(100) not null,
    "order" smallint default 0
);

drop table if exists filters cascade;
create table filters(
    id serial primary key,
    name varchar(100) not null
);

drop table if exists images cascade;
create table images(
    id serial primary key,
    url varchar(200) not null,
    name varchar(100)
);

drop table if exists ingredient_types cascade;
create table ingredient_types(
    id serial primary key,
    name varchar(100) not null
);
insert into ingredient_types (id, name) values
(1, 'Légumes'),
(2, 'Viandes');

drop table if exists ingredients cascade;
create table ingredients(
    id serial primary key,
    name varchar(100) not null,
    type int
        references ingredient_types (id)
);
insert into ingredients (id, name, type) values
(1, 'Carottes', 1),
(2, 'Steack haché', 2);

drop table if exists ingredients_allergens cascade;
create table ingredients_allergens(
    allergen int not null
        references allergens (id),
    ingredient int not null
        references ingredients (id),
    primary key (allergen, ingredient)
);
insert into ingredients_allergens (allergen, ingredient) values
(1, 1);

drop table if exists menu_types cascade;
create table menu_types(
    id serial primary key,
    name varchar(100) not null
);
insert into menu_types (id, name) values
(1, 'Végétariens');

drop table if exists menus cascade;
create table menus(
    id serial primary key,
    price numeric not null,
    name varchar(100) not null,
    active boolean default false not null,
    description varchar(100),
    "date" date,
    image int
        references images (id),
    type int not null
        references menu_types (id)
);
insert into menus (id, price, name, active, description, type) values
(1, 12, 'Veggy menu', true, 'Lorem ipsum dolor sit amet', 1);

drop table if exists products cascade;
create table products(
    id serial primary key,
    name varchar(100) not null,
    price numeric not null,
    weight numeric,
    description varchar(500),
    "date" date,
    image varchar(200)
);
insert into products (name, price, image) values
('Steak-frites', 12, 'http://blob-harmony.groupetva.ca/harmony/media/static/filemanager/content/1454475600/steak-frites-et-sel-d-e-pices_1454532079.jpg'),
('Boeuf bourgignon', 12.3, 'http://www.renaud-viandes.fr/wp-content/uploads/2013/12/boeuf-bourguignon-e1388240462119.jpg'),
('Raclette', 15.99, 'https://gluttodigest.com/wp-content/uploads/2017/02/raclette_main.jpg'),
('Moules-frites', 6.90, 'http://s-www.estrepublicain.fr/images/f5948cd3-c258-4dce-95ef-a4d88e5d4e1a/BES_06/illustration-soiree-moules-frites_1-1477902523.jpg'),
('Blanquette de veau', 18.90, 'http://p1.pic.akm.vodst.com/1289/1289.recette-blanquette-de-veau-minute.w_1280.h_720.m_zoom.c_middle.ts_1354019012..jpg'),
('Côte de boeuf', 21.90, 'https://tastet.ca/wp-content/uploads/2016/05/meilleures-cote-de-boeuf-de-montreal.jpg');

drop table if exists menus_products cascade;
create table menus_products(
    menu int not null
        references menus (id),
    product int not null
        references products (id),
    quantity int default 1 not null,
    primary key (menu, product)
);
insert into menus_products (menu, product) values
(1, 1);

drop table if exists products_categories cascade;
create table products_categories(
    product int not null
        references products (id),
    categorie int not null
    -- TODO : voir la clé étrangère
        --references
);

drop table if exists products_filters cascade;
create table products_filters(
    product int not null
        references products (id),
    filter int not null
        references filters (id),
    primary key (product, filter)
);

drop table if exists products_ingredients cascade;
create table products_ingredients(
    product int not null
        references products (id),
    ingredient int not null
        references ingredients (id),
    primary key (product, ingredient)
);
insert into products_ingredients (product, ingredient) values
(1, 1);

drop table if exists restaurants cascade;
create table restaurants(
    id serial primary key,
    name varchar(100) not null,
    address varchar(100) not null,
    zip varchar(5) not null,
    city varchar(50) not null,
    latitude numeric,
    longitude numeric,
    nb_places int not null,
    description varchar(500),
    phone varchar(8),
    email varchar(100)
);
insert into restaurants (id, name, address, zip, city, nb_places) values
(1, 'Golden resto', '33 avenue de la plaine', '7400', 'Annecy', 200),
(2, 'Com chez toi', '5 ter aenue des trois fontaines', '74600', 'Seynod', 20);

drop table if exists rooms cascade;
create table rooms(
    id serial primary key,
    name varchar(100) not null,
    restaurant int not null
        references restaurants (id)
);

drop table if exists tables cascade;
create table tables(
    id serial primary key,
    number int not null,
    name varchar(100),
    room int not null
        references rooms (id)
);

drop table if exists reservations cascade;
create table reservations(
    id serial primary key,
    nb_peoples int not null,
    firstname varchar(100) not null,
    lastname varchar(100) not null,
    email varchar(100),
    phone varchar(8) not null,
    "table" int not null
        references tables (id),
    date timestamp not null
);

drop table if exists restaurants_menus cascade;
create table restaurants_menus(
    restaurant int not null
        references restaurants (id),
    menu int not null
        references menus (id),
    primary key (restaurant, menu)
);

drop table if exists schedules cascade;
create table schedules(
    id serial primary key,
    start_morning time,
    end_morning time,
    start_afternoon time,
    end_afternoon time,
    date smallint not null,
    restaurant int not null
        references restaurants (id)
);

drop table if exists users cascade;
create table users(
    id serial primary key,
    login varchar(100) not null,
    email varchar(100) not null,
    password varchar(255) not null,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    role smallint default 1 not null
);

drop table if exists users_restaurants cascade;
create table users_restaurants(
    "user" int not null
        references users (id),
    restaurant int not null
        references restaurants (id),
    primary key("user", restaurant)
);